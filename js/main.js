import Task from "./models/task.js";
import { callAPI } from "./utils/callAPI.js";

const getEle = (id) => document.getElementById(id);

// Tạo listTask
const createList = (list) => {
  let content = "";
  for (let i in list) {
    content += `
      <li>
          ${list[i].textTask}
          <div class="buttons">
              <button class="remove" onclick="handleDeleteTask('${list[i].id}')"><i class="far fa-trash-alt"></i></button>
              <button class="complete" onclick="handleCompleteTask('${list[i].id}')"><i class="far fa-check-circle"></i></button>
          </div>
      </li>
      `;
  }
  getEle("todo").innerHTML = content;
  getEle("newTask").value = "";
};

const createListCompleted = (list) => {
  let contentCompleted = "";
  for (let i in list) {
    contentCompleted += `<li>${list[i].textTask}
                  <div class="buttons">
                      <button class="remove" onclick="handleDeleteTask('${list[i].id}')"><i class="far fa-trash-alt"></i></button>
                      <button class="complete" onclick="handleUncompleteTask('${list[i].id}')"><i class="fas fa-check-circle"></i></button>
                  </div>
                </li>`;
  }

  getEle("completed").innerHTML = contentCompleted;
};

const showListTask = () => {
  const listTaskToDo = [];
  const listTaskComplete = [];
  // Hiện loader và ẩn đi nd của todo & comp
  getEle("loader").style.display = "block";
  getEle("todo").style.display = "none";
  getEle("completed").style.display = "none";

  callAPI("textTask", "GET", null)
    .then((res) => {
      // Tắt loader & hiện todo comp lên
      getEle("loader").style.display = "none";
      getEle("todo").style.display = "block";
      getEle("completed").style.display = "block";
      for (let i in res.data) {
        if (res.data[i].status === "todo") {
          listTaskToDo.push(res.data[i]);
        }
        if (res.data[i].status === "complete") {
          listTaskComplete.push(res.data[i]);
        }
      }
      console.log(listTaskToDo);
      console.log(listTaskComplete);
      createList(listTaskToDo);
      createListCompleted(listTaskComplete);
    })
    .catch((err) => console.log(err));
};

showListTask();

//Thêm task mới vào
getEle("addItem").addEventListener("click", () => {
  let nameTask = getEle("newTask").value;

  const newTask = new Task("", nameTask, "todo");
  callAPI("textTask", "POST", newTask)
    .then(() => showListTask())
    .catch((err) => console.log(err));
});

// call axios lấy task theo id
const getInfoTask = (id) => {
  return callAPI(`textTask/${id}`, "GET", null)
    .then((res) => res.data)
    .catch((err) => console.log(err));
};

// lấy task todo theo id rồi chuyển sang complete
const handleCompleteTask = async (id) => {
  const updateTask = await getInfoTask(id);
  updateTask.status = "complete";
  // console.log(id);
  callAPI(`textTask/${updateTask.id}`, "PUT", updateTask)
    .then(() => {
      showListTask();
    })
    .catch((err) => console.log(err));
};

// lấy task complete theo id rồi chuyển sang todo
const handleUncompleteTask = async (id) => {
  const updateTask = await getInfoTask(id);
  updateTask.status = "todo";
  callAPI(`textTask/${updateTask.id}`, "PUT", updateTask)
    .then(() => {
      showListTask();
    })
    .catch((err) => console.log(err));
};

//Xóa task
const handleDeleteTask = (id) => {
  callAPI(`textTask/${id}`, "DELETE", null)
    .then(() => showListTask())
    .catch((err) => console.log(err));
};

window.handleCompleteTask = handleCompleteTask;
window.handleUncompleteTask = handleUncompleteTask;
window.handleDeleteTask = handleDeleteTask;
