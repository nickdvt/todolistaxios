import { API_URL } from "../config/constants.js";

export const callAPI = (uri, method, data) => {
  return axios({
    url: `${API_URL}/${uri}`,
    method,
    data,
  });
};
